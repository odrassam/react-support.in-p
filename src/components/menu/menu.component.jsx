import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import { ListItemText, Icon, CircularProgress } from "@material-ui/core";
import "./menu.styles.scss";
import { auth } from '../../firebase/firebase.util';
import { connect } from 'react-redux';
import { logoutKos } from '../../redux/kos-card/card.actions';
import history from '../../History';
 function SimpleMenu(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }
  function handleClose() {
    setAnchorEl(null);
  }
  function logOut()  {
    props.logoutKos();
    localStorage.clear();
    handleClose();
    history.push('/');
  }
  return (
    <div>
      {console.log(auth.currentUser)}
      <div className="app-bar__popup">
        <IconButton
          onClick={handleClick}
          edge="start"
          style={{ padding: "7px" }}
        >
          <Avatar>{auth.currentUser ? auth.currentUser.email[0].toUpperCase() : <CircularProgress size={10}/>}</Avatar>
        </IconButton>
        <div className="app-bar__avatar-name">
          <Typography variant="body2">{auth.currentUser ? auth.currentUser.email : 'Loading...'}</Typography>
        </div>
      </div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{ marginTop: '40px'}}
      >
        <MenuItem onClick={handleClose} className="pdl-small" style={{ marginTop: 5}}>
          <Icon>account_circle</Icon>
          <ListItemText className="pdl-small" primary="Profile saya"/>
        </MenuItem>
        <MenuItem className="pdl-small" onClick={handleClose} style={{ marginTop: 5 }}>
          <Icon>vpn_key</Icon>
          <ListItemText className="pdl-small" primary="Ubah Password" />
        </MenuItem>
        <MenuItem  className="pdl-small" onClick={logOut} style={{ marginTop: 5 }}>
          <Icon>exit_to_app</Icon>
          <ListItemText className="pdl-small" primary="Logout" />
        </MenuItem>
      </Menu>
    </div>
  );
}

export default connect(null, { logoutKos })(SimpleMenu)