// import React from "react";
// import "./s.main-menu.styles.scss";
// import { Typography, CircularProgress } from "@material-ui/core";
// import CardItem from "../s-card-item/card.item.component";
// import { connect } from "react-redux";
// import { getKosanAll } from "../../redux/kos-card/card.actions";
// const isAuth = JSON.parse(localStorage.getItem("uid"));
// class MainMenu extends React.Component {
//   componentWillMount(){
//     getKosanAll();
//   }
//   componentWillUnmount(){
//     getKosanAll();
//   }
//   renderItem = () => {
//     const { listKos } = this.props;
//     if (!listKos) {
//       return <CircularProgress />;
//     }
//     return listKos.map((cur, key) => {
//       return (
//         cur && (
//           <CardItem
//             key={key}
//             displayNone={isAuth !== cur.userId ? true : false}
//             imageUrl={cur.imageUrl}
//             title={cur.nama_kos}
//             harga={cur.harga_kos}
//             kategori={cur.kategori_kosan}
//             id={cur.id}
//             ukuran={cur.ukuran_kamar}
//           />
//         )
//       );
//     });
//   };
//   render() {
//     return (
//       <div className="s-main-menu">
//         <Typography className="card-title" variant="h6"></Typography>
//         <div className="card-content">{this.renderItem()}</div>
//       </div>
//     );
//   }
// }

// const mapStateToProps = state => {
//   return {
//     listKos: Object.values(state.kosCard)
//   };
// };
// export default connect(
//   mapStateToProps,
//   { getKosanAll }
// )(MainMenu);

import React from "react";
import "./s.main-menu.styles.scss";
import { Typography, CircularProgress } from "@material-ui/core";
import CardItem from "../s-card-item/card.item.component";
import { connect } from "react-redux";
import { getKosanAll } from "../../redux/kos-card/card.actions";
const MainMenu = ({ getKosanAll, listKos }) => {
  React.useEffect(() => {
    getKosanAll();
    return () => {
      listKos = [];
    }
  }, []);
  const isAuth = JSON.parse(localStorage.getItem("uid"));
  const renderItem = () => {
    if (!listKos) {
      return <CircularProgress />;
    }
    return listKos.map((cur, key) => {
      return (
        cur && (
          <CardItem
            key={key}
            displayNone={isAuth !== cur.userId ? true : false}
            imageUrl={cur.imageUrl}
            title={cur.nama_kos}
            harga={cur.harga_kos}
            kategori={cur.kategori_kosan}
            id={cur.id}
            ukuran={cur.ukuran_kamar}
          />
        )
      );
    });
  };
  return (
    <div className="s-main-menu">
      <Typography className="card-title" variant="h6"></Typography>
      <div className="card-content">{renderItem()}</div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    listKos: Object.values(state.kosCard)
  };
};
export default connect(
  mapStateToProps,
  { getKosanAll }
)(MainMenu);
