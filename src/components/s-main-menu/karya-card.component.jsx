import React from "react";
import "./s.main-menu.styles.scss";
import { Typography, CircularProgress } from "@material-ui/core";
import CardItem from "../s-card-item/card.item.component";
import { connect } from "react-redux";
import { getKosanAll } from "../../redux/kos-card/card.actions";
import Dongeng from "../../assets/boy-dongeng.jpg";
import Origami from "../../assets/boy-origami.jpg";
import Senja from "../../assets/boy-senja.jpg";
const listCard = [
  {
    imageUrl: Origami,
    title: 'Origami Hati',
    deskripsiSingkat:
      "Janganlah kamu berpaling dan membuat kia menjadi dua orang asing yang tidak mengenal satu sama lain. di bumi ini, ada banyak sekali orang yang bisa merebutmu"
  },
  {
    imageUrl: Dongeng,
    title: 'Dongeng-Dongeng Yang Tak Utuh',
    deskripsiSingkat:
      "Tuhan juga menciptakan surga di kedua tatap matamu dan di semua bentuk ketidakwarasanku terhadap kamu"
  },
  {
    imageUrl: Senja,
    title: 'Pada Senja Yang Membawamu Pergi',
    deskripsiSingkat:
      "Malam dan kengan sering membuat sebuah bentuk rindu yang seharusnya tidak kuulang. meringis tajam di dadaku"
  }
];
const MainMenu = ({ getKosanAll, listKos }) => {
  React.useEffect(() => {
    getKosanAll();
    return () => {
      listKos = [];
    };
  }, []);
  const isAuth = JSON.parse(localStorage.getItem("uid"));
  const renderItem = () => {
    return listCard.map((cur, key) => {
      return (
        cur && (
          <CardItem
            key={key}
            displayNone={false}
            imageUrl={cur.imageUrl}
            deskripsi={cur.deskripsiSingkat}
            title={cur.title}
            // title={cur.judul}
            // harga={cur.harga}
            // kategori={cur.kategori_kosan}
            // id={cur.id}
            // ukuran={cur.ukuran_kamar}
          />
        )
      );
    });
  };
  return (
    <div className="s-main-menu">
      <Typography className="card-title" variant="h6"></Typography>
      <div className="card-content">{renderItem()}</div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    listKos: Object.values(state.kosCard)
  };
};
export default connect(
  mapStateToProps,
  { getKosanAll }
)(MainMenu);
