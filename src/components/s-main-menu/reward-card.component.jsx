import React from "react";
import "./s.main-menu.styles.scss";
import { Typography, CircularProgress } from "@material-ui/core";
import CardItem from "../s-card-item/card.item.component";
import { connect } from "react-redux";
import { getKosanAll } from "../../redux/kos-card/card.actions";
import Reward1 from '../../assets/reward-1.jpg';
import Reward2 from '../../assets/reward-3.jpg';
const listCard = [
  {
    imageUrl: Reward1,
    title:'Meet and greet bareng Boy Candra',
    point: '100pts',
    deskripsiSingkat:
      "Diskon 70%, dateng meet and greet !"
  },
  {
    imageUrl: Reward2,
    title:'Tanda Tangani Bukumu',
    point:'100pts',
    deskripsiSingkat:
      "Diskon 70% buku Boy Chandra, dengan tanda tanganya"
  },
];
const MainMenu = ({ getKosanAll, listKos }) => {
  React.useEffect(() => {
    getKosanAll();
    return () => {
      listKos = [];
    };
  }, []);
  const isAuth = JSON.parse(localStorage.getItem("uid"));
  const renderItem = () => {
    return listCard.map((cur, key) => {
      return (
        cur && (
          <CardItem
            key={key}
            displayNone={false}
            imageUrl={cur.imageUrl}
            deskripsi={cur.deskripsiSingkat}
            point={cur.point}
            title={cur.title}
            // title={cur.judul}
            // harga={cur.harga}
            // kategori={cur.kategori_kosan}
            // id={cur.id}
            // ukuran={cur.ukuran_kamar}
          />
        )
      );
    });
  };
  return (
    <div className="s-main-menu">
      <Typography className="card-title" variant="h6"></Typography>
      <div className="card-content">{renderItem()}</div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    listKos: Object.values(state.kosCard)
  };
};
export default connect(
  mapStateToProps,
  { getKosanAll }
)(MainMenu);
