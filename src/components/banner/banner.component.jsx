import React from "react";
import { Typography } from "@material-ui/core";
import './banner.styles.scss';
const BannerSingle = ({ image, title }) => {
  return (
    <div className="section-banner-single">
      <div className="section-banner-single__image">
        <img src={image} alt="" style={{ height: "100%", width: "100%" }} />
      </div>
      <Typography variant="h5" className="section-banner-single__title">
        {title}
      </Typography>
    </div>
  );
};

export default BannerSingle;
