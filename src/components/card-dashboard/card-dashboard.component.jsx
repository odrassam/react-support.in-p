import React from "react";
import "./card-dashboard.styles.scss";
import Frame from "../../components/frame/frame.component";
import Footer from "../../components/footer/footer.component";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const Home = props => {
  const width = props.width || '360px';
  return (
    <div className="home-card">
      <Card className="home-card__wrapper" style={{ width: width }}>
        <CardContent className="home-card__content">
          <Typography component="h5" variant="h5">
            {props.title}
          </Typography>
          <Typography variant="subtitle1" color="textSecondary">
            {props.subtitle}
          </Typography>
        </CardContent>
        <CardMedia className="home-card__image" image={props.image} />
      </Card>
    </div>
  );
};

export default Home;
