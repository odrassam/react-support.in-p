import React from "react";
import {
  TextField,
  Button,
  CircularProgress,
  FormControl,
  MenuItem,
  Select,
  InputLabel
} from "@material-ui/core";
import { Field, reduxForm } from "redux-form";
import ImageUploader from "../image-uploader/image.component";
import "./kos-forms.styles.scss";
const renderInput = ({ input, label, css, placeholder, type = "text" }) => {
  return (
    <TextField
      variant="outlined"
      required
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      type={type}
    />
  );
};
const renderTextArea = ({ input, label, css, placeholder }) => {
  return (
    <TextField
      variant="outlined"
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      multiline={true}
      rows={2}
      rowsMax={Infinity}
      required
    />
  );
};
const Advance = [
  ["wifi_tethering", "Wifi"],
  ["toys", "Kipas Angin"],
  ["tv", "Televisi"],
  ["bathtub", "Kamar Mandi Dalam"],
  ["room_service", "Dapat Makanan"],
  ["people", "Muat 2 orang"]
];
const medium = [
  ["wifi_tethering", "Wifi"],
  ["room_service", "Dapat Makanan"],
  ["people", "Muat 2 orang"]
];
const sederhana = [
  ["signal_wifi_off", "Tidak ada wifi"],
  ["cloud_off", "Tidak ada Pelayanan"]
];
const renderSelect = ({ input, css }) => {
  return (
    <FormControl variant="outlined" className={css}>
      <InputLabel htmlFor="outlined-age-simple">Fasilitas Kos</InputLabel>
      <Select
        labelWidth={100}
        inputProps={{
          name: "age",
          id: "outlined-age-simple"
        }}
        {...input}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        <MenuItem value={Advance}>
          Kos Elit (Wifi, Kipas angin, Tv, Bathub, Kamar Mandi, Makan, Muat 2
          orang)
        </MenuItem>
        <MenuItem value={medium}>
          Kos Menengah (Wifi, makanan, muat 2 orang)
        </MenuItem>
        <MenuItem value={sederhana}>
          Kos Biasa (Tidak ada wifi, Tidak ada pelayanan)
        </MenuItem>
      </Select>
    </FormControl>
  );
};
const renderSelect2 = ({ input, css }) => {
  return (
    <FormControl variant="outlined" className={css}>
      <InputLabel htmlFor="outlined-age-simple">Tempat Kosan</InputLabel>
      <Select
        labelWidth={100}
        inputProps={{
          name: "age",
          id: "outlined-age-simple"
        }}
        {...input}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        <MenuItem value="sawojajar">Sawojajar</MenuItem>
        <MenuItem value="kepanjen">Kepanjen</MenuItem>
        <MenuItem value="sukun">Sukun</MenuItem>
        <MenuItem value="singosari">Singosari</MenuItem>
        <MenuItem value="blimbing">Blimbing</MenuItem>
        <MenuItem value="klojen">Klojen</MenuItem>
      </Select>
    </FormControl>
  );
};
const KosForms = ({ handleSubmit, submitValue, imageUrl, message }) => {
  const [visibility, setVisibility] = React.useState(false);
  const onSubmit = formValues => {
    submitValue(formValues);
    setVisibility(true);
    setTimeout(() => {
      setVisibility(false);
    }, 3000);
  };
  return (
    <React.Fragment>
      <form className="section-add__fields" onSubmit={handleSubmit(onSubmit)}>
        <Field
          name="nama_kos"
          component={renderInput}
          placeholder="Kos Cantik"
          label="Nama Kosanmu !"
          css="field-1"
        />
        <Field
          name="tempat_kos"
          component={renderSelect2}
          css="field-2"
          type="text"
        />
        <Field
          name="harga_kos"
          component={renderInput}
          placeholder="100000"
          label="Harga Kosanmu/bulan"
          css="field-2"
          type="number"
        />
        <Field
          name="ukuran_kamar"
          component={renderInput}
          placeholder="3x4"
          label="Ukuran Kamar"
          css="field-2"
          type="text"
        />
        <Field
          name="kategori_kosan"
          component={renderInput}
          placeholder="Campur/Laki2/Perempuan"
          label="Katergori Kosan"
          css="field-2"
          type="text"
        />
        <Field
          name="fasilitas_kos"
          component={renderSelect}
          css="field-2"
          type="text"
        />
        <Field
          name="no_telp"
          component={renderInput}
          placeholder="628223347**** (Tanpa '+')"
          label="Nomor Hp/WA"
          css="field-2"
          type="number"
        />
        <Field
          name="alamat_lengkap_kos"
          component={renderTextArea}
          placeholder="Dekat Alfamart....."
          label="Alamat Lengkap"
          css="field-3"
        />
        <Field
          name="tentang_kos"
          component={renderTextArea}
          placeholder="Kosku......."
          label="Deskripsi Kosanmu !"
          css="field-3"
        />
        <ImageUploader image={imageUrl} message={message} />
        <Button
          type="submit"
          style={{ marginBottom: "20px" }}
          color="primary"
          variant="contained"
        >
          {visibility ? (
            <CircularProgress size={20} style={{ color: "white" }} />
          ) : (
            "Publikasi Kosmu !"
          )}
        </Button>
      </form>
    </React.Fragment>
  );
};

const FormHOC = reduxForm({
  form: "KosForm"
})(KosForms);

export default FormHOC;
