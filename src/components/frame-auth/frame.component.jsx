import React from "react";
import "./frame.styles.scss";
import { Typography } from "@material-ui/core";
import Notif from '../snackbar/snackbar.component';
import history from '../../History';
const FrameAuth = props => {
  const onChecked = () => {
    const isAuth = JSON.parse(localStorage.getItem("uid"));
    if(isAuth){
      history.push(`/kos/owned`);
    }
  }
  React.useEffect(() => {
    onChecked();
  }, [])
  return (
    <div className="page-login">
      <Notif message="Cek kembali email/password anda !"/>
      <div className="section-input">
        <div className="section-input__logo">
          <Typography variant="h5" className="logo-title">
            <span className="logo-title--1">Support.in,</span> <br />
            <span className="logo-title--2">{props.title}</span>
          </Typography>
        </div>
        {props.children}
      </div>
      <div className="section-img">
        <img className="section-img__image" src={props.banner} alt="" />
      </div>
    </div>
  );
};

export default FrameAuth;
