import React from "react";
import "./footer.styles.scss";
import { Typography } from "@material-ui/core";
const Footer = () => (
  <div className="footer">
    <Typography>&copy;Support.in Dev 2019</Typography>
  </div>
);

export default Footer;
