import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TempImage from "../../assets/boy-dongeng.jpg";
import { Typography } from "@material-ui/core";
import "./modal-specific.styles.scss";
export default function AlertDialog(props) {
  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
        <DialogContent className="dialog-content">
          <img src={props.image} className="dialog-content__image" />
          <DialogContentText
            id="alert-dialog-description"
            className="dialog-content__description"
          >
            {props.deskripsi}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.handleClose} color="secondary">
            Delete
          </Button>
          <Button onClick={props.handleClose} color="primary">Update</Button>
          <Button
            onClick={props.handleClose}
            variant="contained"
            color="primary"
            autoFocus
          >
            Back
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
