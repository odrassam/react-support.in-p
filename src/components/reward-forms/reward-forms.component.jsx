import React from "react";
import { TextField, Button, CircularProgress } from "@material-ui/core";
import { Field, reduxForm } from "redux-form";
import ImageUploader from "../image-uploader/image.component";
import "./reward-forms.styles.scss";
import { Link } from "react-router-dom";
const renderInput = ({ input, label, css, placeholder, type = "text" }) => {
  return (
    <TextField
      variant="outlined"
      required
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      type={type}
    />
  );
};
const renderTextArea = ({ input, label, css, placeholder }) => {
  return (
    <TextField
      variant="outlined"
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      multiline={true}
      rows={2}
      rowsMax={Infinity}
      required
    />
  );
};

const KosForms = ({ handleSubmit, submitValue, imageUrl, message }) => {
  const [visibility, setVisibility] = React.useState(false);
  const onSubmit = formValues => {
    submitValue(formValues);
    setVisibility(true);
    setTimeout(() => {
      setVisibility(false);
    }, 3000);
  };
  return (
    <React.Fragment>
      <form className="section-add__fields" onSubmit={handleSubmit(onSubmit)}>
        <Field
          name="nama_reward"
          component={renderInput}
          placeholder="Nama Reward"
          label="Contoh: Online Koala Kumal PDF"
          css="field-1"
        />
        <Field
          name="harga_pts"
          component={renderInput}
          placeholder="100pts"
          label="Requirement"
          css="field-2"
          type="text"
        />
        <Field
          name="link_reward"
          component={renderInput}
          placeholder="https://google.com"
          label="Link Rewardmu"
          css="field-2"
          type="text"
        />
        <Field
          name="deskripsi_reward"
          component={renderTextArea}
          placeholder="Online pdf koala kumal..."
          label="Deskripsi Reward"
          css="field-3"
        />
        <ImageUploader image={imageUrl} message={message} />
        <Link style={{ textDecoration: 'none'}} to="/reward/owned">
          <Button
            type="submit"
            style={{ marginBottom: "20px" }}
            color="primary"
            variant="contained"
          >
            {visibility ? (
              <CircularProgress size={20} style={{ color: "white" }} />
            ) : (
              "Buat Rewardmu !"
            )}
          </Button>
        </Link>
      </form>
    </React.Fragment>
  );
};

const FormHOC = reduxForm({
  form: "KosForm"
})(KosForms);

export default FormHOC;
