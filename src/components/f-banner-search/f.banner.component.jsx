import React from "react";
import "./f.banner.styles.scss";
import BoyTemp from "../../assets/boy-dongeng.jpg";
import BgYellow from '../../assets/bg-yellow.jpeg';
import BgGrey from '../../assets/bg-grey.jpeg';
import BgBlue from '../../assets/bg-blue.jpeg';
import { Typography } from "@material-ui/core";
import CardDashboard from "../card-dashboard/card-dashboard.component";
const Banner = () => (
  <div className="banner-front">
    <div className="banner-header">
      <div className="banner-title">
        <Typography variant="h5" className="banner-title--1">
          Boy Candra
        </Typography>
        <Typography variant="subtitle1" className="banner-title--2">
          Penulis
        </Typography>
        <Typography variant="subtitle2" className="banner-title--3">
          ( Belum Ada Sosial Media )
        </Typography>
      </div>
      <img src={BoyTemp} alt="Avatar" className="banner-avatar" />
    </div>
    <div className="profil-card">
      <CardDashboard
        title="Penyumbang"
        subtitle="100 Orang"
        image={BgYellow}
        width="300px"
      />
      <CardDashboard
        title="Pendapatan"
        subtitle="Rp.90jt"
        image={BgBlue}
        width="300px"
      />
      <CardDashboard
        title="Karya"
        subtitle="100 Karya"
        image={BgGrey}
        width="300px"
      />
    </div>
    <div className="profil-description">
      <div className="profil-header">
        <Typography variant="h5" className="profil-header--title">
          Tentang Saya
        </Typography>
        <hr className="profil-header--divider" />
      </div>
      <div className="profil-about">
        <Typography variant="body1" className="profil-about--text">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis ad
          nostrum quaerat vitae optio, corporis harum repellendus? Error,
          provident. Dolorem repudiandae veniam ullam aut corrupti adipisci rem.
          Laudantium doloribus consequatur ducimus optio! Natus nesciunt nulla
          accusantium sapiente saepe laborum ipsum vitae, id aspernatur
          obcaecati qui. Sint saepe veniam dignissimos et!
        </Typography>
      </div>
    </div>
  
  </div>
);

export default Banner;
