import React from "react";
import "./f.side-menu.styles.scss";
import { Typography, Icon, ButtonBase } from "@material-ui/core";
import { connect } from 'react-redux';
import { logoutKos } from '../../redux/kos-card/card.actions';

import { Link } from "react-router-dom";
import { auth } from "../../firebase/firebase.util";
import history from "../../History";

const SideMenu = props => {
  const signOut = () => {
    props.logoutKos();
    localStorage.clear();
    auth.signOut();
    history.push("/");
  };
  return (
    <div className="side-menu">
      <Link to={`/home`} style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">dashboard</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Dashboard
          </Typography>
        </ButtonBase>
      </Link>
      <Link to={`/karya/tambah`} style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">book</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Tambah Karya
          </Typography>
        </ButtonBase>
      </Link>
      <Link to={`/karya/owned`} style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">bookmarks</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Daftar Karya
          </Typography>
        </ButtonBase>
      </Link>
      <Link to={`/reward/tambah`} style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">card_giftcard</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Tambah Reward
          </Typography>
        </ButtonBase>
      </Link>
      <Link to={`/reward/owned`} style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">loyalty</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Daftar Reward
          </Typography>
        </ButtonBase>
      </Link>
      <Link to={`/profile`} style={{ textDecoration: "none" }}>
        <ButtonBase disableRipple={true} className="side-menu--item">
          <Icon className="side-menu--item__icon">account_box</Icon>
          <Typography variant="h5" className="side-menu--item__title">
            Profile
          </Typography>
        </ButtonBase>
      </Link>
      <ButtonBase
        onClick={signOut}
        disableRipple={true}
        className="side-menu--item"
      >
        <Icon className="side-menu--item__icon">exit_to_app</Icon>
        <Typography variant="h5" className="side-menu--item__title">
          Logout
        </Typography>
      </ButtonBase>
    </div>
  );
};

export default connect(null, { logoutKos })(SideMenu);
