import React from "react";
import { Typography, Icon, Button } from "@material-ui/core";
import Banner from "../banner/banner.component";
import { connect } from "react-redux";
import { getKosan } from "../../redux/kos-card/card.actions";
const FasilitasArticle = props => (
  <div className="kos-content--fasilitas">
    <Typography variant="h6" className="kos-content-title">
      {props.title}
    </Typography>
    <div className="kos-item">
      <Typography className="kos-content-fasilitas">
        <Icon className="kos-content-fasilitas__logo">aspect_ratio</Icon>
        {props.content}
      </Typography>
    </div>
  </div>
);
const FasilitasKosan = props => (
  <div className="kos-content--fasilitas">
    <Typography variant="h6" className="kos-content-title">
      {props.title}
    </Typography>
    {props.content &&
      props.content.map((cur, key) => {
        return (
          <div className="kos-item" key={key}>
            <Typography className="kos-content-fasilitas">
              <Icon className="kos-content-fasilitas__logo">{cur[0]}</Icon>
              {cur[1]}
            </Typography>
          </div>
        );
      })}
  </div>
);
const DeskripsiArticle = props => (
  <div className="kos-content--jenis-kos">
    <Typography className="kos-content-title" variant="h6">
      {props.title}
    </Typography>
    <Typography className="kos-content-jenis-kos" variant="body1">
      {props.tentang_kos}
    </Typography>
  </div>
);

const KosArticles = ({ params, getKosan, kosan }) => {
  React.useEffect(() => {
    getKosan(params);
  }, []);
  return (
    <React.Fragment>
      {kosan && <Banner image={kosan.imageUrl} title={kosan.nama_kos} />}
      <div className="kos-content">
        {kosan && (
          <React.Fragment>
            <DeskripsiArticle
              title="Jenis Kosan"
              tentang_kos={kosan.kategori_kosan}
            />
            <DeskripsiArticle
              title="Wilayah Kosan"
              tentang_kos={kosan.tempat_kos}
            />
            <DeskripsiArticle
              title="Harga Kosan"
              tentang_kos={`Rp.${kosan.harga_kos}/bulan`}
            />
            <FasilitasArticle title="Luas Kamar" content={kosan.ukuran_kamar} />
            <FasilitasKosan
              title="Fasilitas Kosan"
              content={kosan.fasilitas_kos}
            />
            <DeskripsiArticle
              title="Alamat Lengkap Kosan"
              tentang_kos={kosan.alamat_lengkap_kos}
            />
            <DeskripsiArticle
              title="Deskripsi Kosan"
              tentang_kos={kosan.tentang_kos}
            />
            <Button
              target="__blank"
              variant="contained"
              color="primary"
              style={{ marginTop: "50px" }}
              href="https://api.whatsapp.com/send?phone=6282233476938"
            >
              WA Sekarang
            </Button>
          </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
};
const mapStateToProps = (state, ownProps) => {
  return {
    kosan: state.kosCard[ownProps.params]
  };
};
export default connect(
  mapStateToProps,
  { getKosan }
)(KosArticles);
