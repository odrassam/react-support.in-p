import React from "react";
import "./frame.styles.scss";
import Header from "../header/header.component";
import SideMenu from "../f-side-menu/f.side-menu.component";
import Snackbar from "../snackbar/snackbar.component";
import history from "../../History";
const Frame = props => {
  const checkUid = () => {
    const isAuth = JSON.parse(localStorage.getItem("uid"));
    if (!isAuth) {
      history.push("/");
    }
  };
  React.useEffect(() => {
    checkUid();
  }, []);
  return (
    <React.Fragment>
      <Header />
      {/* <Snackbar message="Ada Kesalahan !" /> */}
      <div className="front-page-content">
        <div className="front-page-content__side-menu">
          <SideMenu />
        </div>
        <div className="front-page-content__main-menu">{props.children}</div>
      </div>
    </React.Fragment>
  );
};

export default Frame;
