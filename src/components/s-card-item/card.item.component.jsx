import React from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
// import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
// import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
// import TemporaryImg from "../../assets/lokasi-1.jpg";
import { Link } from "react-router-dom";
import SpecificModal from "../modal-specific/modal-specific.component";
import "./card.styles.scss";
const CardItem = props => {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div className={`card-item ${props.displayNone ? "dissapear" : ""}`}>
      <SpecificModal
        open={open}
        handleClose={handleClose}
        image={props.imageUrl}
        deskripsi={props.deskripsi}
        title={props.title}
      />
      <Card style={{ maxWidth: 340, height: 380 }}>
        <CardActionArea onClick={handleClickOpen}>
          {/* <Link to={`/kos/single/${props.id}`} style={{ textDecoration: "none" }}> */}
          <CardMedia
            image={props.imageUrl}
            title="Contemplative Reptile"
            style={{ height: 150 }}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {props.title}
            </Typography>
            <Typography
              gutterBottom
              variant="body1"
              color="textSecondary"
              component="p"
            >
              {props.deskripsi}
            </Typography>
            <Typography
              style={{ marginTop: 10 }}
              gutterBottom
              variant="subtitle2"
              color="textSecondary"
              component="p"
            >
              {props.point}
            </Typography>
            {/* <Typography variant="body2" color="textSecondary" component="p">
                {`Kategori Kosan : ${props.kategori}`}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {`Ukuran Kamar : ${props.ukuran}`}
              </Typography> */}
          </CardContent>
          {/* </Link> */}
        </CardActionArea>
        {/* <CardActions>
          <Link to={`/kos/single/${props.id}`} style={{ textDecoration: 'none'}}>
            <Button variant="outlined" size="small" color="primary">
              Lihat
            </Button>
          </Link>
          <Link to={`/kos/delete/${props.id}`} style={{ textDecoration: 'none'}}>
            <Button variant="outlined" size="small" color="secondary">
              Delete
            </Button>
          </Link>
          <Link to={`/kos/update/${props.id}`} style={{ textDecoration: 'none'}}>
            <Button variant="outlined" size="small" style={{ color: 'yellow', borderColor: 'yellow'}}>
              Update
            </Button>
          </Link>
        </CardActions> */}
      </Card>
    </div>
  );
};

export default CardItem;
