import React from "react";
import "./image.styles.scss";
import ImageUploader from "react-images-upload";
import { connect } from "react-redux";
import { getLocalImage, setImageData } from "../../redux/kos-card/card.actions";
import BannerComponent from "../banner/banner.component";

const Uploader = ({ getLocalImage, LocalImageUrl, setImageData, image, message }) => {
  const onChangeImage = async props => {
    const localUrl = URL.createObjectURL(props[props.length - 1]);
    setImageData(props[props.length - 1]);
    getLocalImage(localUrl);
  };
  const renderImage = () => {
    let showImage = LocalImageUrl || image;
    return showImage && <BannerComponent image={showImage} title="" />;
  };
  const pesan = message || 'Input Banner';
  return (
    <div className="image-uploader">
      {renderImage()}
      <ImageUploader
        withIcon={true}
        buttonText={pesan}
        onChange={onChangeImage}
        imgExtension={[".jpg", ".gif", ".png", ".gif"]}
        maxFileSize={5242880}
        singleImage={true}
        fileContainerStyle={{
          width: "300px",
          boxShadow: "0 5px 10px rgba(0,0,0,.2)"
        }}
        required
      />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    LocalImageUrl: state.kosCard.localImageUrl
  };
};
export default connect(
  mapStateToProps,
  { getLocalImage, setImageData }
)(Uploader);
