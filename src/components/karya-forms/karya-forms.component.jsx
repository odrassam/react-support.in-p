import React from "react";
import { TextField, Button, CircularProgress } from "@material-ui/core";
import { Field, reduxForm } from "redux-form";
import ImageUploader from "../image-uploader/image.component";
import "./karya-forms.styles.scss";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { clearImage } from '../../redux/kos-card/card.actions';
const renderInput = ({ input, label, css, placeholder, type = "text" }) => {
  return (
    <TextField
      variant="outlined"
      required
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      type={type}
    />
  );
};
const renderTextArea = ({ input, label, css, placeholder }) => {
  return (
    <TextField
      variant="outlined"
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={css}
      {...input}
      multiline={true}
      rows={2}
      rowsMax={Infinity}
      required
    />
  );
};

const KosForms = ({ handleSubmit, submitValue, imageUrl, message, clearImage }) => {
  const [visibility, setVisibility] = React.useState(false);
  const onSubmit = formValues => {
    // submitValue(formValues);
    console.log(formValues);
    setVisibility(true);
    setTimeout(() => {
      setVisibility(false);
    }, 3000);
  };
  const clearData = () => {
    clearImage();
    onSubmit();
  }
  return (
    <React.Fragment>
      <form className="section-add__fields" onSubmit={handleSubmit(onSubmit)}>
        <Field
          name="nama_karya"
          component={renderInput}
          placeholder="Koala Kumal"
          label="Nama Karyamu"
          css="field-1"
        />
        <Field
          name="tahun_dibuat"
          component={renderInput}
          placeholder="2017"
          label="Tahun Penerbitan"
          css="field-2"
          type="number"
        />
        <Field
          name="link_karya"
          component={renderInput}
          placeholder="https://google.com"
          label="Link Karyamu"
          css="field-2"
          type="text"
        />
        <Field
          name="deskripsi_karya"
          component={renderTextArea}
          placeholder="Koala kumal merupakan ceritaku masa muda....."
          label="Deskripsi Karya"
          css="field-3"
        />
        <ImageUploader image={imageUrl} message={message} />
        {/* <Link style={{ textDecoration: "none" }} to="/karya/owned"> */}
          <Button
            type="submit"
            style={{ marginBottom: "20px" }}
            color="primary"
            variant="contained"
            onClick={clearData}
          >
            {visibility ? (
              <CircularProgress size={20} style={{ color: "white" }} />
            ) : (
              "Publikasi Karyamu !"
            )}
          </Button>
        {/* </Link> */}
      </form>
    </React.Fragment>
  );
};

const FormHOC = reduxForm({
  form: "KosForm"
})(KosForms);

export default connect(null, { clearImage })(FormHOC);
