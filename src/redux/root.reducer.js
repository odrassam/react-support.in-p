import { combineReducers } from 'redux';
import { reducer as FormReducer} from 'redux-form';
import KosCardReducer from '../redux/kos-card/card.reducer';
import Types from  './kos-card/card.types';
const appReducer =  combineReducers({
  form: FormReducer,
  kosCard: KosCardReducer,
})

const rootReducer = (state, action) => {
  if(action.type == Types.LOGOUT_KOS){
    state = undefined;
  }
  return appReducer(state, action);
}

export default rootReducer;