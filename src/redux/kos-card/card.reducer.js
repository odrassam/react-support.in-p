import Types from "./card.types";
import _ from 'lodash';
const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.GET_IMAGE_LOCAL:
      return {
        ...state,
        localImageUrl: action.payload
      };
    case Types.TEMP_CLEAR:
      return {
        ...state,
        localImageUrl: null,
      }
    case Types.GET_IMAGE:
      return {
        ...state,
        imageData: action.payload
      };
    case Types.CREATE_KOS:
      return {
        ...state,
        kosData: action.payload,
        localImageUrl: null,
        error: false
      };
    case Types.GET_KOS_ALL:
      return {
        ...state,
        ..._.mapKeys(action.payload, "id")
      }
    case Types.GET_KOS:
      return{
        ...state,
        [action.payload.id]:action.payload
      }
    case Types.UPDATE_KOS:
      return{
        ...state,
        [action.payload.id]:action.payload
      }
    case Types.DELETE_KOS:
      return _.omit(state, action.payload)
    case Types.GET_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case Types.LOGIN_KOS:
      return action.payload;
    case Types.REGISTER_KOS:
        return action.payload;
    default:
      return state;
  }
};
