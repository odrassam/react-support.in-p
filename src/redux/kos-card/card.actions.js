import Types from "./card.types";
import Cloudinary from "../api/cloudinary";
import Kosan from "../api/kos";
import history from "../../History";
import { auth } from "../../firebase/firebase.util";
export const getLocalImage = images => {
  return {
    type: Types.GET_IMAGE_LOCAL,
    payload: images
  };
};
export const setImageData = images => {
  return {
    type: Types.GET_IMAGE,
    payload: images
  };
};
export const registerKos = formValues => async dispatch => {
  const { email, password } = formValues;
  try {
    const response = await auth.createUserWithEmailAndPassword(email, password);
    dispatch({
      type: Types.REGISTER_KOS,
      payload: response
    });
    history.push("/");
  } catch (e) {
    console.log(e.code);
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const loginKos = formValues => async dispatch => {
  const { email, password } = formValues;
  try {
    const response = await auth.signInWithEmailAndPassword(email, password);
    localStorage.setItem("uid", JSON.stringify(response.user.uid));
    dispatch({
      type: Types.LOGIN_KOS,
      payload: { uid: response.user.uid, email: response.user.email }
    });
    history.push(`/karya/tambah`);
  } catch (e) {
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const logoutKos = () => {
  return {
    type: Types.LOGOUT_KOS
  };
};
export const closeError = () => ({
  type: Types.GET_ERROR,
  payload: false
});
export const clearImage = () => {
  return {
    type: Types.TEMP_CLEAR,
    payload: false,
  }
}
export const createKosan = (formValues, id) => async (dispatch, getState) => {
  const { imageData } = getState().kosCard;
  const form = new FormData();
  form.append("file", imageData);
  form.append("upload_preset", "lsvfxa7q");
  try {
    const responseImage = await Cloudinary.post("/upload", form);
    const response = await Kosan.post("/kosan", {
      ...formValues,
      userId: id,
      imageUrl: responseImage.data.secure_url
    });
    dispatch({
      type: Types.CREATE_KOS,
      payload: response.data
    });
    history.push(`/kos/owned`);
  } catch (e) {
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const getKosan = id => async dispatch => {
  try {
    const response = await Kosan.get(`/kosan/${id}`);
    console.log(response);
    dispatch({
      type: Types.GET_KOS,
      payload: response.data
    });
  } catch (e) {
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const getKosanAll = () => async dispatch => {
  try {
    const response = await Kosan.get("/kosan");
    dispatch({
      type: Types.GET_KOS_ALL,
      payload: response.data
    });
  } catch (e) {
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const updateKosan = (formValues, id) => async (dispatch, getState) => {
  const { imageData } = getState().kosCard;
  const form = new FormData();
  form.append("file", imageData);
  form.append("upload_preset", "lsvfxa7q");
  try {
    const responseImage = await Cloudinary.post("/upload", form);
    const response = await Kosan.patch(`/kosan/${id}`, {
      ...formValues,
      imageUrl: responseImage.data.secure_url
    });
    console.log(response);
    dispatch({
      type: Types.UPDATE_KOS,
      payload: response.data
    });
    history.push(`/kos/owned`);
  } catch (e) {
    dispatch({
      type: Types.GET_ERROR,
      payload: true
    });
  }
};
export const deleteKosan = id => async dispatch => {
  await Kosan.delete(`/kosan/${id}`);
  dispatch({
    type: Types.DELETE_KOS,
    payload: id
  });
  history.push(`/kos/owned`);
};
