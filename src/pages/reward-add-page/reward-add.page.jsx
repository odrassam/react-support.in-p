import React from "react";
import "./reward.styles.scss";
import Frame from "../../components/frame/frame.component";
import { Typography } from "@material-ui/core";
import Footer from "../../components/footer/footer.component";
import RewardForms from "../../components/reward-forms/reward-forms.component";
import { connect } from "react-redux";
// import { createKosan } from "../../redux/kos-card/card.actions";
const KosAdd = props => {
  const isAuth = JSON.parse(localStorage.getItem("uid"));
  const onSubmit = formValues => {
    props.createKosan(formValues, isAuth);
  };
  return (
    <Frame>
      <React.Fragment>
        <div className="section-add">
          <div className="section-add__banner">
            <Typography variant="h5" className="banner-title">
              Buat reward untuk fans mu !
            </Typography>
          </div>
          <div>
            <RewardForms submitValue={onSubmit} />
          </div>
          <Footer />
        </div>
      </React.Fragment>
    </Frame>
  );
};

export default connect(null)(KosAdd);
