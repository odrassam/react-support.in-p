import React from "react";
import "./home.styles.scss";
import Frame from "../../components/frame/frame.component";
import Footer from "../../components/footer/footer.component";
import Card from "../../components/card-dashboard/card-dashboard.component";
import TempImage from "../../assets/boy-dongeng.jpg";
import BasicTable  from '../../components/data-table/data-table.component';
import BgYellow from '../../assets/bg-yellow.jpeg';
import BgBlue from '../../assets/bg-blue.jpeg';
import BgGrey from '../../assets/bg-grey.jpeg';
const Home = props => {
  return (
    <Frame>
      <div className="section-home">
        <div className="home-side-1">
          <Card title="Penyumbang" subtitle="100 Orang" image={BgYellow} />
          <Card title="Pendapatan" subtitle="Rp.10jt" image={BgBlue} />
          <Card title="Karya" subtitle="100 Karya" image={BgGrey} />
        </div>
        <div className="home-side-2">
          <BasicTable className="home-side-2__component"/>
        </div>
      </div>
      <Footer />
    </Frame>
  );
};

export default Home;
