import React from "react";
import "./kos-add.styles.scss";
import Frame from "../../components/frame/frame.component";
import { Typography, CircularProgress } from "@material-ui/core";
import Footer from "../../components/footer/footer.component";
import KosForms from "../../components/kos-forms/kos-forms.component";
import { connect } from "react-redux";
import { getKosan, updateKosan } from "../../redux/kos-card/card.actions";
import _ from "lodash";
const ReuseComponent = props => {
  return (
    <Frame>
      <div className="section-add">
        <div className="section-add__banner">
          <Typography variant="h5" className="banner-title">
            Iklankan Kosmu disini !
          </Typography>
        </div>
        <div>{props.children}</div>
        <Footer />
      </div>
    </Frame>
  );
};
const isAuth = JSON.parse(localStorage.getItem("uid"));
const KosUpdate = props => {
  const getData = () => {
    props.getKosan(isAuth);
  };
  const onSubmit = formValues => {
    console.log(formValues)
    props.updateKosan(formValues, props.match.params.id);
  };
  React.useEffect(() => {
    getData();
  }, []);
  const renderComponent = () => {
    if (!props.kosan) {
      return (
        <ReuseComponent>
          <CircularProgress />
        </ReuseComponent>
      );
    }
    return (
      <ReuseComponent>
        <KosForms
          submitValue={onSubmit}
          initialValues={_.pick(
            props.kosan,
            "nama_karya",
            "tahun_dibuat",
            "link_karya",
            "deskripsi_karya",
          )}
          imageUrl={props.kosan.imageUrl}
          message="Tetap inputkan banner yang sama !"
        />
      </ReuseComponent>
    );
  };
  return <React.Fragment>{renderComponent()}</React.Fragment>;
};

const mapStateToProps = (state, ownProps) => {
  return {
    kosan: state.kosCard[ownProps.match.params.id]
  };
};
export default connect(
  mapStateToProps,
  { getKosan, updateKosan }
)(KosUpdate);
