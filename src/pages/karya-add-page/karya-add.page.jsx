import React from "react";
import './karya-page.styles.scss';
import Frame from "../../components/frame/frame.component";
import { Typography } from "@material-ui/core";
import Footer from "../../components/footer/footer.component";
import KaryaForms from "../../components/karya-forms/karya-forms.component";
import { connect } from "react-redux";
// import { createKosan } from "../../redux/kos-card/card.actions";
const KosAdd = props => {
  const isAuth = JSON.parse(localStorage.getItem("uid"));
  const onSubmit = formValues => {
    props.createKosan(formValues, isAuth);
  };
  return (
    <Frame>
      <React.Fragment>
        <div className="section-add">
          <div className="section-add__banner-3">
            <Typography variant="h5" className="banner-title">
              Tambahkan Karyamu disini !
            </Typography>
          </div>
          <div>
            <KaryaForms submitValue={onSubmit}/>
          </div>
          <Footer />
        </div>
      </React.Fragment>
    </Frame>
  );
};

export default connect(null)(KosAdd);
