import React, { useEffect } from "react";
import Modal from "../../components/modal/modal.component";
import { connect } from "react-redux";
import { deleteKosan, getKosan} from "../../redux/kos-card/card.actions";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
const DeleteTrotoar = props => {
  useEffect(() => {
    props.getKosan(props.match.params.id);
  }, []);
  const isAuth = JSON.parse(localStorage.getItem("uid"));
  const renderActions = () => {
    return (
      <React.Fragment>
        <Button
          color="secondary"
          onClick={() => props.deleteKosan(props.match.params.id)}
        >
          Delete
        </Button>

        <Link style={{ textDecoration: "none" }} to={`/kos/owned`}>
          <Button color="primary" autoFocus>
            Cancel
          </Button>
        </Link>
      </React.Fragment>
    );
  };
  const renderContent = () => {
    if (!props.kosan) {
      return "Do you want to delete this item ?";
    }
    return `Do you want to delete this item : ${props.kosan.nama_kos}`;
  };
  return (
    <div>
      <Modal
        title="Are you sure to delete this ?"
        action={renderActions()}
        content={renderContent()}
      />
    </div>
  );
};
const mapStateToProps = (state, ownProps) => {
  return {
    kosan: state.kosCard[ownProps.match.params.id]
  };
};
export default connect(
  mapStateToProps,
  { getKosan, deleteKosan }
)(DeleteTrotoar);
