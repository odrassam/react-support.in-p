import React from "react";
import "./kos-add.styles.scss";
import Frame from "../../components/frame/frame.component";
import { Typography } from "@material-ui/core";
import Footer from "../../components/footer/footer.component";
import KosForms from "../../components/kos-forms/kos-forms.component";
import { connect } from "react-redux";
import { createKosan } from "../../redux/kos-card/card.actions";
const KosAdd = props => {
  const isAuth = JSON.parse(localStorage.getItem("uid"));
  const onSubmit = formValues => {
    props.createKosan(formValues, isAuth);
  }
  return (
    <Frame>
      <React.Fragment>
        <div className="section-add">
          <div className="section-add__banner">
            <Typography variant="h5" className="banner-title">
              Iklankan Kosmu disini !
            </Typography>
          </div>
          <div>
            <KosForms submitValue={onSubmit} />
          </div>
          <Footer />
        </div>
      </React.Fragment>
    </Frame>
  );
};

export default connect(
  null,
  { createKosan }
)(KosAdd);
