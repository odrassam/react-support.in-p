import React from "react";
import MainProfile from '../../components/f-main-menu/f.main-menu.component';
import Frame from '../../components/frame/frame.component';
import "./profil-owned.styles.scss";

const FrontPage = () => {
  return (
    <Frame>
      <MainProfile/>
    </Frame>
  );
};

export default FrontPage;
