import React from "react";
import {
  TextField,
  Button,
  Typography,
  CircularProgress,
  FormControl,
  MenuItem,
  Select,
  InputLabel
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import { registerKos } from "../../redux/kos-card/card.actions";
import { connect } from "react-redux";
import FrameAuth from "../../components/frame-auth/frame.component";
import Banner4 from "../../assets/banner-4.png";
const RenderInput = ({ input, label, placeholder, type, meta }) => {
  const error = meta.touched && meta.error ? true : false;
  console.log(meta);
  return (
    <TextField
      required
      error={error}
      label={label}
      placeholder={placeholder}
      type={type}
      InputLabelProps={{ shrink: true }}
      style={{ marginBottom: "25px", width: "300px" }}
      helperText={error ? meta.error : ""}
      {...input}
    />
  );
};
const RenderSelect = ({ input, classes }) => {
  return (
    <FormControl className={classes}>
      <InputLabel htmlFor="outlined-age-simple">Tempat Kosan</InputLabel>
      <Select
        labelWidth={100}
        inputProps={{
          name: "age",
          id: "outlined-age-simple"
        }}
        {...input}
      >
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
        <MenuItem value="Penulis">Penulis</MenuItem>
        <MenuItem value="Konten Kreator">Konten Kreator</MenuItem>
        <MenuItem value="Developer">Developer</MenuItem>
        <MenuItem value="Seniman">Seniman</MenuItem>
      </Select>
    </FormControl>
  );
};
const renderTextArea = ({ input, label, classes, placeholder }) => {
  return (
    <TextField
      variant="outlined"
      label={label}
      placeholder={placeholder}
      InputLabelProps={{
        shrink: true
      }}
      className={classes}
      {...input}
      multiline={true}
      rows={2}
      rowsMax={Infinity}
      required
    />
  );
};
const RegisterAuth = props => {
  const [visibility, setVisibility] = React.useState(false);
  const onSubmit = formValues => {
    setVisibility(true);
    setTimeout(() => {
      setVisibility(false);
    }, 3000);
    props.registerKos(formValues);
  };
  return (
    <FrameAuth
      onSubmit={props.handleSubmit(onSubmit)}
      banner={Banner4}
      title="Register here !"
    >
      <form
        className="section-input__text-field"
        onSubmit={props.handleSubmit(onSubmit)}
      >
        <Field
          name="name"
          label="Username"
          placeholder="Username"
          type="text"
          classes="text-field--1"
          component={RenderInput}
        />
        <Field
          name="email"
          label="Email"
          placeholder="jhon@gmail.com"
          type="email"
          classes="text-field--1"
          component={RenderInput}
        />
        <Field
          name="description"
          label="Deksripsi Dirimu"
          placeholder="Deskripsi Dirimu"
          type="text"
          classes="text-field--1"
          component={renderTextArea}
        />
        <Field
          name="category"
          label="Category"
          placeholder="belum memilih"
          type="text"
          classes="text-field--1"
          component={RenderSelect}
        />
        <Field
          name="description"
          label="Deksripsi Dirimu"
          placeholder="Deskripsi Dirimu"
          type="text"
          classes="text-field--1"
          component={renderTextArea}
        />
        <Field
          name="password"
          label="Password"
          placeholder="Password"
          type="password"
          classes="text-field--1"
          component={RenderInput}
        />
        <Field
          name="password_confirmation"
          label="Confirm Password"
          placeholder="Password"
          type="password"
          classes="text-field--1"
          component={RenderInput}
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          style={{ marginTop: 30 }}
        >
          {visibility ? (
            <CircularProgress size={20} style={{ color: "white" }} />
          ) : (
            "Register"
          )}
        </Button>
        <Typography
          variant="subtitle2"
          style={{ fontWeight: "300", marginTop: "10px" }}
        >
          Already have an account, login <Link to="/">here</Link>
        </Typography>
      </form>
    </FrameAuth>
  );
};

const validate = formValues => {
  const errors = {};
  if (formValues.password !== formValues.password_confirmation) {
    errors.password_confirmation = "Password did not match";
  }
  return errors;
};
const formHOC = reduxForm({
  form: "Login",
  validate
})(RegisterAuth);

export default connect(null, { registerKos })(formHOC);
