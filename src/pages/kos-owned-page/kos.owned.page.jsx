import React from 'react';
import './kos.styles.scss';
import MainMenu from "../../components/s-main-menu/s.main-menu.component";
import Frame from '../../components/frame/frame.component';
import Footer from '../../components/footer/footer.component';

const KosOwned = props => {
  return (
    <Frame>
      <MainMenu params={props.match.params.id}/>
      <Footer/>
    </Frame>
  )
}

export default KosOwned;