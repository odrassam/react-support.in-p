import React from "react";
import Frame from "../../components/frame/frame.component";
import Footer from "../../components/footer/footer.component";
import Banner from "../../components/banner/banner.component";
import TempImage from "../../assets/banner-2.jpg";
import ArticleComponent from "../../components/kos-articles/kos-articles.component";
import "./kos-single.styles.scss";

const SinglePage = props => {
  return (
    <Frame>
      <div className="section-single-kos">
        <ArticleComponent params={props.match.params.id}/>
      </div>
      <Footer />
    </Frame>
  );
};

export default SinglePage;
