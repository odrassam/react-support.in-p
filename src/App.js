import React from "react";
import "./App.styles.scss";
import { Router, Route } from "react-router-dom";
import history from "./History";
import KosTambahPage from "./pages/kos-add-page/kos-add.page";
import KosUpdatePage from "./pages/kos-add-page/kos-update.page";
import KosDeletePage from "./pages/kos-add-page/kos-delete.page";
import KosOwnedPage from "./pages/kos-owned-page/kos.owned.page";
import KosLoginPage from "./pages/kos-auth-page/kos-auth.login.page";
import KosRegisterPage from "./pages/kos-auth-page/kos-auth.register.page";
import KosSinglePage from "./pages/kos-single-page/kos-single.page";
// TEMP
import KaryaTambahPage from './pages/karya-add-page/karya-add.page';
import KaryaOwnedPage from './pages/karya-owned-page/karya-owned.page';
import RewardTambahPage from './pages/reward-add-page/reward-add.page';
import RewardOwnedPage from './pages/reward-owned-page/reward-owned.page';
import Home from './pages/home/home.page';
import ProfilePage from './pages/profil-owned-page/profil-owned.page';
import SpecificPage from './components/modal-specific/modal-specific.component';

class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Route path="/" exact component={KosLoginPage} />
        <Route path="/register" exact component={KosRegisterPage} />
        <Route path="/kos/tambah" exact component={RewardTambahPage} />
        <Route path="/kos/update/:id" exact component={KosUpdatePage} />
        <Route path="/kos/delete/:id" exact component={KosDeletePage} />
        <Route path="/kos/owned" exact component={KosOwnedPage} />
        <Route path="/kos/single/:id" exact component={KosSinglePage} />


        {/* <Route path="/" exact component={KosLoginPage} /> */}
        {/* <Route path="/register" exact component={KosRegisterPage} /> */}
        
        <Route path="/reward/tambah" exact component={RewardTambahPage} />
        <Route path="/reward/update/:id" exact component={KosUpdatePage} />
        <Route path="/reward/delete/:id" exact component={KosDeletePage} />
        <Route path="/reward/owned" exact component={RewardOwnedPage} />

        <Route path="/karya/tambah" exact component={KaryaTambahPage} />
        <Route path="/karya/update/:id" exact component={KosUpdatePage} />
        <Route path="/karya/delete/:id" exact component={KosDeletePage} />
        <Route path="/karya/owned" exact component={KaryaOwnedPage} />

        <Route path="/profil/tambah" exact component={KosTambahPage} />
        <Route path="/profil/update/:id" exact component={KosUpdatePage} />
        <Route path="/profil/delete/:id" exact component={KosDeletePage} />
        <Route path="/profile" exact component={ProfilePage} />
        <Route path="/home" exact component={Home}/>
      </Router>
    );
  }
}

export default App;
