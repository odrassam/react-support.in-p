import React from "react";
import { Route, Redirect } from "react-router-dom";
export const Auth = ({ path, component: Component, isAuth }) => {
  return (
    <Route
      path={path}
      exact
      render={routerProps => {
        if (isAuth) {
          return <Component {...routerProps} />;
        }
        return <Redirect to="/" />;
      }}
    />
  );
};

export const IfAuth = ({ path, component: Component, isAuth, uid }) => {
  return (
    <Route
      path={path}
      exact
      render={routerProps => {
        if (isAuth) {
          return <Redirect to={`/kos/owned/${uid}`} />;
        }
        return <Component {...routerProps}/>;
      }}
    />
  );
};