import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyABJWaGqil_lbvTuUnhv3I9qKM7EVGQCOs",
  authDomain: "crwn-db-bea01.firebaseapp.com",
  databaseURL: "https://crwn-db-bea01.firebaseio.com",
  projectId: "crwn-db-bea01",
  storageBucket: "crwn-db-bea01.appspot.com",
  messagingSenderId: "813263512283",
  appId: "1:813263512283:web:aa3d54ba472ecb80"
};

firebase.initializeApp(config);

export const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);
export default firebase;